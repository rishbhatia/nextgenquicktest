// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add("login", (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add("drag", { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add("dismiss", { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This will overwrite an existing command --
// Cypress.Commands.overwrite("visit", (originalFn, url, options) => { ... })

Cypress.Commands.add("SiteAdminLogin", (Username, Password) => { 
    cy.get('#username').click()
    cy.get('#username').type('Rishi.Bhatia@levelaccess.com')
    cy.get('#password').click()
    cy.get('#password').type('Artielange55!')
    cy.wait(3000)
    cy.get('#kc-login').click({ force:true })
    cy.wait(4000)
}) 


Cypress.Commands.add("ClickFilter", (FilterButton)=> {
    cy.get('.MuiButton-label').click({force:true})
 })
 
 Cypress.Commands.add("ResetAllFilters",(Status,Statusid, IndusryGrp, IndustryGrpid, Industry, IndustryId, Company, Companyid  )=>{
    cy.wait(2000)
    cy.get('#status-filter').click();
    cy.wait(2000)
    cy.get('li[data-value=\'All\']').click();
    //Industry Group Reset 
    cy.get('form.MuiGrid-root > div:nth-of-type(3) svg:nth-of-type(1)').click({force:true});
    cy.get('li[data-option-index=\'0\']').click({force:true});
    //Industry Reset 
    cy.get('form.MuiGrid-root > div:nth-of-type(4) svg:nth-of-type(1)').click({force:true});
    cy.get('li[data-option-index=\'0\']').click({force:true});   
    // Group Reset 
    cy.get('div.MuiGrid-grid-xs-11 path:nth-of-type(1)').click({force:true})
    cy.get('li[data-option-index=\'0\']').click({force:true})
 })
 