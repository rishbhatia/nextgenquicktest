/// <reference types="cypress" />

context('Example Tests with Continuum', () => {

    // See `continuum/cypress.js` for the definitions of custom commands like `cy.setUpContinuum` and the rest

    before(() => {
        // Continuum only needs to be set up once per testing context;
        // the page under test can change without having to set up Continuum again
        cy.setUpContinuum('continuum/continuum.conf.js')
    })

    it('Browse Level Access.com', () => {
        cy.visit('https://apps.aamc.org/first-gloc-web/#/organizer')
        cy.get('.mat-checkbox-inner-container').click()
        cy.wait(1000)
        cy.get('.mat-raised-button').click()
        .runAllAccessibilityTests()
        .printAccessibilityTestResults()
            // to send these accessibility concerns from Continuum to AMP, uncomment the line below and
            // edit the submitAccessibilityConcernsToAMP function in `continuum/cypress.js` according to our
            // 'Sending Continuum Testing Results to AMP' support article:
            // https://support.levelaccess.com/hc/en-us/articles/360024510632-Sending-Continuum-Testing-Results-to-AMP
            //.submitAccessibilityConcernsToAMP()
            //.get('a').first().click()
            //.runAllAccessibilityTests()
            //.printAccessibilityTestResults()
            //.submitAccessibilityConcernsToAMP()
            //.failIfAnyAccessibilityConcerns()
    })
})
