
let  date  = require ('date-and-time');
   
   context('Location', () => {

    before(() => {
      cy.visit('https://admin-integration.levelaccess.io/');
      cy.wait(3000)
      cy.SiteAdminLogin({email: 'Rishi.Bhatia@levelaccess.com', password: 'Artielange55!'})
      
      cy.get(':nth-of-type(5) .MuiTypography-root').click()
      cy.get('[name="Resources"] > .MuiListItemText-root > .MuiTypography-root').click() 
    
    })
  
   it('Add a Resource', () => {
    cy.viewport('macbook-15')
    cy.contains('resourceType').should('be.visible')
    cy.contains('resourceMapping').should('be.visible')
    cy.contains('resourceAction').should('be.visible')
    cy.contains('group').should('be.visible')
    cy.contains('permission').should('be.visible') 
    cy.get('.MuiBreadcrumbs-ol').should('be.visible');
    //Verification the Add Button is present
    cy.get('[style="flex-grow: 1;"] > .MuiButtonBase-root').should('be.visible');
    // Verification Top Row Pagination is present 
    cy.get('.MuiPaper-root > .MuiGrid-container').should('be.visible');    

  })
  
  it('Clicking on the Add Button ', () => {
    let d = date.format(new Date(), 'MM-DD-YYYY');
   
    cy.get('.fa-plus').click()
    cy.get('#name').type('CypressResource'+ d)
    
  })
  






 
})