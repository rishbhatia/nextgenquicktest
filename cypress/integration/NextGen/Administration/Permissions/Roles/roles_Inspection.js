

   context('Location', () => {

    before(() => {
      cy.visit('https://admin-integration.levelaccess.io/');
      cy.wait(3000)
      cy.SiteAdminLogin({email: 'Rishi.Bhatia@levelaccess.com', password: 'Artielange55!'})
      cy.contains('Permissions').click()
      //cy.get(':nth-child(6) > .MuiListItemText-root > .MuiTypography-root').click()

      cy.get('div[name=\'Roles\'] .MuiTypography-root').click()
      
    
    })
  
   it('Inspect the Resource Mapping Page', () => {
    cy.viewport('macbook-15')
    cy.contains('Actions').should('be.visible')
    cy.contains('Name').should('be.visible')
    cy.contains('Description').should('be.visible')
    cy.contains('Roles').should('be.visible')
    cy.contains('Role Admin').should('be.visible')
    cy.get('.MuiTableHead-root > .MuiTableRow-root > :nth-child()').as('rows')
    cy.get('@rows').first().should('have.text','Actions')

  })
  
  it('Inspect First Table Row', () => {
    cy.contains('Role Admin').should('be.visible')   
     // cy.get('tbody.MuiTableBody-root > tr:nth-of-type(1) > td:nth-of-type(2)').should('have.text','tenants')
     //cy.get('tbody.MuiTableBody-root > tr:nth-of-type(1) > td:nth-of-type(3)').should('have.text','get')
    // cy.get('tbody.MuiTableBody-root > tr:nth-of-type(1) > td:nth-of-type(4)').should('have.text','/tenants')
   //cy.get('tbody.MuiTableBody-root > tr:nth-of-type(1) > td:nth-of-type(5)').should('have.text','tenant:list')
  })  

})