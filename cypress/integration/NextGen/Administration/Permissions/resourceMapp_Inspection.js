
   context('Location', () => {

    before(() => {
      cy.visit('https://admin-integration.levelaccess.io/');
      cy.wait(3000)
      cy.SiteAdminLogin({email: 'Rishi.Bhatia@levelaccess.com', password: 'Artielange55!'})
      cy.get('div:nth-of-type(5) .MuiTypography-root').click()
      cy.get('div[name=\'Resource Mappings\'] .MuiTypography-root').click()
      cy.setUpContinuum('continuum/continuum.conf.js')
       
    
    })
  
   it('Inspect the Resource Mapping Page', () => {
    cy.viewport('macbook-15')
    cy.contains('Resource Mappings').should('be.visible')
    cy.contains('Actions').should('be.visible')
    cy.contains('API').should('be.visible')
    cy.contains('Method').should('be.visible')
    cy.contains('Path').should('be.visible')
    cy.contains('Mapping').should('be.visible')
    .runAllAccessibilityTests()
    .printAccessibilityTestResults()
   


  })
  
  it('Inspect First Table Row', () => {
   cy.get('tbody.MuiTableBody-root > tr:nth-of-type(1) > td:nth-of-type(2)').should('have.text','tenants')
   cy.get('tbody.MuiTableBody-root > tr:nth-of-type(1) > td:nth-of-type(3)').should('have.text','get')
   cy.get('tbody.MuiTableBody-root > tr:nth-of-type(1) > td:nth-of-type(4)').should('have.text','/tenants')
   //cy.get('tbody.MuiTableBody-root > tr:nth-of-type(1) > td:nth-of-type(5)').should('have.text','tenant:list')


  })










  
  
}) 